# Service

This is a base for a service for retrieving a trailer/trailers for movies.

Most of what I wanted is not included in this due to issues with `localstack` and no current working access to AWS. DynamoDB would not work at all on my computer so that part is not working. Spent a bit too long on trying to get `localstack` to work but got a base setup working for this service.

### Main ideas
For a small service, I was planning on adding a simple DynamoDB table where the movie name is stored as the key along with the tmdb id and an array of trailers where one has a main flag set as true to specify that it should be returned. This allows for the possibility of a content moderator to update the main one.

For scaling, I would move the movie trailer retrieval into a SQS queue to make sure no rate limits are hit. In an optimal world, a script would run initially to get all available movies and queue them so the trailers would be available from the get go. A webhook/a notification(SNS) would also optimally be implemented for any new movies added. If initial script is possible, just queueing up and returning empty in the beginning should suffice scale wise but less optimal for the end user.

### Test
##### Prerequisites
- localstack

##### Instructions
Start off with copying the `.env.example` to `.env` and enter the API key for TMDB. Afterwards, run
```
yarn && yarn dev:deploy
# or
npm i && npm run dev:deploy
```
and you will get an endpoint in the logs, take that and add `/trailer/{movie}` where movie could be any movie on Viaplay, e.g. `fargo-1996`. (Example: `http://localhost:4567/restapis/phhalway4f/local/_user_request_/trailer/fargo-1996`)
