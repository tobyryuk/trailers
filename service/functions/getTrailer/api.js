const { TMDB_API_KEY } = process.env
const axios = require('axios')

async function getFromViaplay (movie) {
  const { data: viaplayData } = await axios.get(`https://content.viaplay.se/pc-se/film/${movie}`)
  const imdbID = viaplayData._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id
  const tmdbID = await getTmdbID(imdbID)
  const trailers = await getTrailersFromTmdb(tmdbID)
  return { tmdbID, trailers }
}

async function getTrailersFromTmdb (tmdbID) {
  const { data } = await axios.get(`https://api.themoviedb.org/3/movie/${tmdbID}?api_key=${TMDB_API_KEY}&append_to_response=videos`)
  return data.videos.results.filter(video => video.type === 'Trailer').map((trailer, index) => {
    return {
      main: index === 0 ? true : false,
      link: `https://youtu.be/${trailer.key}`
    }
  })
}

async function getTmdbID (imdbID) {
  const { data } = await axios.get(`https://api.themoviedb.org/3/find/${imdbID}?api_key=${TMDB_API_KEY}&external_source=imdb_id`)
  return data.movie_results[0].id
}

module.exports = {
  getFromViaplay
}
