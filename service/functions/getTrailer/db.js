const { TABLE } = process.env
const dynamodb = new (require('aws-sdk/clients/dynamodb'))({
  apiVersion: '2012-08-10',
  // endpoint: 'http://localhost:4569',
  region: 'us-east-1'
})

async function get (url) {
  const params = {
    Key: {
     'movie': {
       S: url
      }
    },
    TableName: TABLE
   }

  try {
    return await dynamodb.getItem(params).promise()
  } catch (e) {
    return false
  }
}

async function put (movie, tmdbID, trailers) {
  const params = {
    Item: {
     'movie': {
       S: movie
      },
     'tmdbID': {
       S: `${tmdbID}`
      },
     'trailers': {
       S: JSON.stringify(trailers)
      }
    },
    TableName: TABLE
   }

  try {
    return await dynamodb.putItem(params).promise()
  } catch (e) {
    console.log(e)
  }
}

module.exports = {
  get,
  put
}
