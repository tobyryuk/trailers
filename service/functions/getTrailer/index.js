const api = require('./api.js')
const db = require('./db.js')

module.exports.handle = async event => {
  if (!event.pathParameters.movie) {
    return {
      statusCode: 400,
      body: JSON.stringify(
        {
          error: 'Missing movie path parameter'
        },
        null,
        2
      )
    }
  }

  const movie = event.pathParameters.movie
  const dbItem = await db.get(movie)
  let trailer = ''

  if (dbItem) {
    // Set trailer from dbItem
  } else {
    const movieData = await api.getFromViaplay(movie)
    await db.put(movie, movieData.tmdbID, movieData.trailers)
    trailer = movieData.trailers.filter(trailer => trailer.main)[0].link
  }


  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        trailer
      },
      null,
      2
    )
  }
}
