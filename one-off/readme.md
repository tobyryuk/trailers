# One Off

A one off script for getting the URL to a trailer for the given movie.

This is not meant to scale but to play around with the API and getting comfortable with it.

To run this script, start off with copying the `.env.example` to `.env` and enter the API key for TMDB. After that just simply run:
```
yarn && node index.js
# or
npm i && node index.js
```
