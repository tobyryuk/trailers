require('dotenv').config()
const fetch = require('node-fetch')
const TMDB_API_KEY = process.env.TMDB_API_KEY

async function getTrailerForMovie(url) {
  const imdbID = (
    await (fetch(url).then(response => response.json()))
  )._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id

  const tmdbID = (
    await (fetch(`https://api.themoviedb.org/3/find/${imdbID}?api_key=${TMDB_API_KEY}&external_source=imdb_id`).then(response => response.json()))
  ).movie_results[0].id

  const trailers = (
    await(fetch(`https://api.themoviedb.org/3/movie/${tmdbID}?api_key=${TMDB_API_KEY}&append_to_response=videos`).then(response => response.json()))
  ).videos.results.filter(video => video.type === 'Trailer')

  return `https://youtu.be/${trailers[0].key}`
}

(async () => {
  console.log(await getTrailerForMovie('https://content.viaplay.se/pc-se/film/fargo-1996'))
})()
